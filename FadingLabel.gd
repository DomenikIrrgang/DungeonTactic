extends Label

export(float) var speed = 0.7

signal label_faded(label)

var fading = true

func _process(delta):
	if (fading):
		modulate.a -= speed * delta
	if (modulate.a <= 0):
		fading = false
		emit_signal("label_faded", self)
