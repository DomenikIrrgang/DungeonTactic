extends Sprite

export(bool) var inverted

func set_flip_h(flip_h):
	flip_h = flip_h
	
func get_flip_h():
	return flip_h
	
func flip():
	flip_h = !flip_h
	
func get_width() -> float:
	return self.texture.get_size().x  / self.hframes
	
func get_height() -> float:
	return self.texture.get_size().y / self.vframes
	
func get_center() -> Vector2:
	return Vector2(
		self.position.x + get_width() / 2,
		self.position.y - get_height() / 2
	)
