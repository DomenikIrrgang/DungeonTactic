extends Node2D

const ResourceChange = preload("res://elements/combat/ResourceChange.tscn")
const SoundEffects = preload("res://audio/Effects.gd")

var animation_state_machine
var sound_effect = AudioStreamPlayer.new()

func _ready():
	animation_state_machine = $AnimationTree.get("parameters/playback")
	animation_state_machine.start("idle")
	add_child(sound_effect)

func set_animation(animation):
	$AnimationTree.get("parameters/playback").travel(animation)
	
func ability_landed(combat_result):
	if (combat_result.hit_type == HitType.DODGED):
		dodge()
	if (combat_result.hit_type == HitType.LANDED or  combat_result.hit_type == HitType.CRITICAL):
		if (combat_result.value >= 0):
			hit()
		else:
			heal()
		show_health_change(combat_result.value, combat_result.hit_type == HitType.CRITICAL)
	else:
		show_unit_message(combat_result.hit_type)
	
func hit():
	set_animation("hit")
	sound_effect.stream = load(SoundEffects.HIT)
	sound_effect.play()

func heal():
	set_animation("heal")
	sound_effect.stream = load(SoundEffects.HIT)
	sound_effect.play()
	
func dodge():
	var isInverted = $Sprite.inverted
	var isFlipped = $Sprite.flip_h 
	if ((!isFlipped and !isInverted) or (isFlipped and isFlipped)):
		set_animation("dodge")
	else:
		set_animation("dodge_enemy")
	
func show_unit_message(message):
	create_message_display().show_message(message)
	
func show_health_change(value, critical):
	create_message_display().show_value(value, critical)
	
func create_message_display():
	var message_display = ResourceChange.instance()
	add_child(message_display)
	#message_display.position = Vector2(get_width() / 2 / get_scale().x, get_height() / 2 / get_scale().y)
	message_display.position = Vector2(0, -(get_height() / 2))
	return message_display
	
func flip():
	$Sprite.flip()

func scale(scale_x, scale_y):
	set_scale(Vector2(scale_x, scale_y))
	
func get_height():
	return $Sprite.get_height()
	
func get_width():
	return $Sprite.get_width()
	
func get_center():
	return Vector2(
		position.x,
		position.y - get_height() / 2
	)
