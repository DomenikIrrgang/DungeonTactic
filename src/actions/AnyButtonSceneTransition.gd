extends Node

export(String, FILE, "*.tscn") var scene
export(bool) var enabled: bool

func _input(event) -> void:
	if (event is InputEventKey or event is InputEventMouseButton) and enabled:
		var scene_change_result = get_tree().change_scene(scene)
		self.enabled = false
		if (scene_change_result == ERR_CANT_OPEN):
			print("Could not changed scene. Scene not found!")
