extends TextureProgress

export(float) var animationSpeed = 1.0

var tween

func _ready():
	tween = Tween.new()
	add_child(tween)

func update_values(new_current_value, new_max_value):
	max_value = new_max_value
	if tween != null:
		tween.interpolate_property(self, 'value', value, new_current_value, animationSpeed, Tween.TRANS_SINE, Tween.EASE_OUT)
		tween.start()
		yield(tween, "tween_completed")
	else:
		value = new_current_value
