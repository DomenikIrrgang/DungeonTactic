extends Light2D

export(float) var maximum_energy
export(float) var minimum_energy
export(float) var minimum_size
export(float) var maximum_size
export(float) var speed = 0.7
export(bool) var fading = true

func _ready():
	if fading == true:
		self.energy = maximum_energy
		self.texture_scale = maximum_size
	else:
		self.energy = minimum_energy
		self.texture_scale = minimum_size

func _process(delta):
	if fading == true:
		self.energy -= speed * delta
		if self.energy <= minimum_energy:
			fading = false
	else:
		self.energy += speed * delta
		if self.energy >= maximum_energy:
			fading = true
	self.texture_scale = (self.energy - minimum_energy) / (maximum_energy - minimum_energy) * (maximum_size - minimum_size) + minimum_size
	
	
