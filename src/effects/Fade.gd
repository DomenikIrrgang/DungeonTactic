extends Label

export(float) var speed = 0.7
export(bool) var fading = true

func _ready():
	if fading == true:
		modulate.a = 1
	else:
		modulate.a = 0

func _process(delta):
	if fading == true:
		modulate.a -= speed * delta
		if modulate.a <= 0:
			fading = false
	else:
		modulate.a += speed * delta
		if modulate.a >= 1:
			fading = true
	
