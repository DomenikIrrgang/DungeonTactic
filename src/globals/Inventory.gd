extends Node

signal inventory_item_added(item, quantity, index)
signal inventory_item_removed(item, quantity, index)
signal inventory_item_placed(item, quantity, index)
signal inventory_item_not_added(item, quantity)

var size = 24

func _init():
	GameState.player_state.inventory.resize(size)
	
func swap_items(index1: int, index2: int) -> void:
	var item1 = get_items()[index1]
	var item2 = get_items()[index2]
	if (item1 != null):
		place_item(item1.item, item1.quantity, index2)
	else:
		get_items()[index2] = null
		emit_signal("inventory_item_placed", null, 0, index2)
	if (item2 != null):
		place_item(item2.item, item2.quantity, index1)
	else:
		get_items()[index1] = null
		emit_signal("inventory_item_placed", null, 0, index1)

func place_item(item: Item, quantity: int, index: int) -> void:
	get_items()[index] = {
		"item": item,
		"quantity": quantity
	}
	emit_signal("inventory_item_placed", item, quantity, index)

func add_item(item: Item, quantity: int) -> int:
	var item_index = has_item(item)
	if (item_index != -1):
		GameState.player_state.inventory[item_index].quantity += quantity
		emit_signal("inventory_item_added", item, quantity, item_index)
		return item_index
	else:
		var new_index = find_first_empty_slot()
		if (new_index != -1):
			GameState.player_state.inventory[new_index] = {
				"item": item,
				"quantity": quantity
			}
			emit_signal("inventory_item_added", item, quantity, new_index)
			return new_index
		else:
			emit_signal("inventory_item_not_added", item, quantity)
			return -1
		
func remove_item(item: Item, quantity: int) -> int:
	var item_index = has_item(item)
	if (item_index >= 0):
		var item_quantity = get_items()[item_index].quantity
		if (item_quantity <= quantity):
			GameState.player_state.inventory[item_index] = null
			emit_signal("inventory_item_removed", item, item_quantity, item_index)
		else:
			GameState.player_state.inventory[item_index].quantity -= quantity
			emit_signal("inventory_item_removed", item, quantity, item_index)
	return item_index
	
func has_item(item: Item) -> int:
	for i in range(0, size):
		if get_items()[i] != null and item.name == get_items()[i].item.name:
			return i
	return -1
	
func get_items() -> Array:
	return GameState.player_state.inventory

func find_first_empty_slot() -> int:
	for i in range(0, size):
		if (get_items()[i] == null):
			return i
	return -1
