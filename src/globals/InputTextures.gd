extends Node

const BASE_PATH = "res://assets/ui/buttons/"

const KEY_ESCAPE = BASE_PATH + "key_escape.png"
const KEY_SPACE = BASE_PATH + "key_space.png"
const GAMEPAD_A = BASE_PATH + "a_button.png"
const GAMEPAD_B = BASE_PATH + "b_button.png"
const GAMEPAD_X = BASE_PATH + "x_button.png"
const GAMEPAD_Y = BASE_PATH + "y_button.png"
