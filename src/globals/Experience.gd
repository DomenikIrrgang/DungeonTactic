extends Node

signal experience_gained(amount)
signal level_up(level)

func increase(amount: int):
	GameState.player_state.experience += amount
	emit_signal("experience_gained", amount)
	if (experience_needed(get_level()) <= GameState.player_state.experience):
		level_up()
		
func get_current_experience() -> int:
	return GameState.player_state.experience
	
func get_current_experience_needed() -> int:
	return experience_needed(get_level())
		
func level_up() -> int:
	if (experience_needed(get_level()) > GameState.player_state.experience):
		GameState.player_state.experience = 0
	else:
		GameState.player_state.experience = GameState.player_state.experience - experience_needed(get_level())
	GameState.player_state.level += 1
	emit_signal("level_up", GameState.player_state.level)
	return GameState.player_state.level

func get_level() -> int:
	return GameState.player_state.level
	
func experience_needed(current_level: int) -> int:
	return current_level * 200
