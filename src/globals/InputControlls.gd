extends Node

const XBOX_BUTTON_INDEX = {
	JOY_XBOX_A: 0,
	JOY_XBOX_B: 1,
	JOY_XBOX_X: 2,
	JOY_XBOX_Y: 3,
	JOY_START: 6,
	JOY_SELECT: 7
}

const KEY_INDEX = {
	KEY_A: 13,
	KEY_B: 14,
	KEY_C: 15,
	KEY_D: 16,
	KEY_E: 17,
	KEY_F: 18,
	KEY_G: 19,
	KEY_H: 20,
	KEY_I: 21,
	KEY_J: 22,
	KEY_K: 23,
	KEY_L: 24,
	KEY_M: 25,
	KEY_N: 26,
	KEY_O: 27,
	KEY_P: 28,
	KEY_Q: 29,
	KEY_R: 30,
	KEY_S: 31,
	KEY_T: 32,
	KEY_U: 33,
	KEY_V: 34,
	KEY_W: 35,
	KEY_X: 36,
	KEY_Y: 37,
	KEY_Z: 38,
	KEY_ENTER: 39,
	KEY_SPACE: 40,
	KEY_ESCAPE: 41,
}

const XBOX_BUTTON_TEXTURE = {
	JOY_XBOX_A: InputTextures.GAMEPAD_A,
	JOY_XBOX_B: InputTextures.GAMEPAD_B,
	JOY_XBOX_X: InputTextures.GAMEPAD_X,
	JOY_XBOX_Y: InputTextures.GAMEPAD_Y,
}

const KEY_TEXTURE = {
	KEY_ESCAPE: InputTextures.KEY_ESCAPE,
	KEY_SPACE: InputTextures.KEY_SPACE,
}

var device_id = -1
var dead_zone = 0.05

func _ready():
	Input.connect("joy_connection_changed", self, "_joy_connection_changed")
	if has_controller():
		self.device_id = Input.get_connected_joypads()[0]
	
func has_controller() -> bool:
	return Input.get_connected_joypads().size() > 0
	
func get_controller_axis_x(default: float) -> float:
	if (has_controller()):
		var x_axis_value = Input.get_joy_axis(device_id, JOY_AXIS_0)
		if (x_axis_value > dead_zone or x_axis_value < -dead_zone):
			return x_axis_value
	return default
	
func get_controller_axis_y(default: float) -> float:
	if (has_controller()):
		var y_axis_value = Input.get_joy_axis(device_id, JOY_AXIS_1)
		if (y_axis_value > dead_zone or y_axis_value < -dead_zone):
			return y_axis_value
	return default
		
func _joy_connection_changed(device_id: int, connected: bool):
	if connected:
		self.device_id = device_id
	else:
		self.device_id = -1

func get_button_texture(action_name: String) -> String:
	for action in InputMap.get_action_list(action_name):
		if action is InputEventKey and !has_controller():
			return KEY_TEXTURE[action.scancode]
		if action is InputEventJoypadButton and has_controller():
			if "XInput" in Input.get_joy_name(device_id):
				return XBOX_BUTTON_TEXTURE[XBOX_BUTTON_INDEX[action.button_index]]
	return InputTextures.GAMEPAD_A

