extends Node
class_name Item

var icon: String

func _init(name: String, icon: String):
	self.name = name
	self.icon = icon
