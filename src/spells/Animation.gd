extends Node2D

signal animation_start()
signal animation_end()

var start_position
var end_position

func run(param_start_position, param_end_position):
	start_position = param_start_position
	end_position = param_end_position
	$Animation.current_animation = "impact"
	position = start_position
	emit_signal("animation_start")
	$Animation.connect("animation_finished", self, "on_animation_finished")
	
func on_animation_finished(animation_name):
	if (animation_name == "impact"):
		$Sprite.visible = false
		emit_signal("animation_end")

func scale(x, y):
	scale.x = x
	scale.y = y
