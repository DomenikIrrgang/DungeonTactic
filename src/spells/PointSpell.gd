extends "./Spell.gd"

func start(combat, ability_cast_result: AbilityCastResult):
	.start(combat, ability_cast_result)
	$Animation.current_animation = "impact"
	position = end_position
