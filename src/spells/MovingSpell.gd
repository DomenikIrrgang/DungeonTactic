extends "./Spell.gd"

var first_travel = true
var tween

func _init():
	tween = Tween.new()
	add_child(tween)

func start(combat, ability_cast_result: AbilityCastResult):
	.start(combat, ability_cast_result)
	start_travel(start_position, end_position)
	tween.connect("tween_completed", self, "on_moving_complete")
	if reflected:
		var reflect_animation = preload("res://animations/abilities/Reflect.tscn").instance()
		get_tree().get_current_scene().add_child(reflect_animation)
		reflect_animation.run(end_position, end_position)
	
func start_travel(start_position, end_position):
	$Animation.current_animation = "moving"
	tween.interpolate_property(self, "position", start_position, end_position, $Animation.current_animation_length, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	
func on_moving_complete(object, key):
	if (reflected == true && first_travel == true):
		first_travel = false
		start_travel(end_position, start_position)
	else:
		$Animation.current_animation = "impact"
