extends "./Spell.gd"

export(PackedScene) var animation

var first_travel = true
var tween
var animations_started = 0
var animations_finished = 0

func _init():
	tween = Tween.new()
	tween.connect("tween_completed", self, "on_moving_complete")
	add_child(tween)

func start(combat, ability_cast_result: AbilityCastResult):
	position = Vector2(0, 0)
	emit_signal("animation_start")
	for cast in ability_cast_result.results:
		var cast_animation = animation.instance()
		cast_animation.connect("animation_end", self, "on_partial_animation_finished")
		animations_started += 1
		combat.start_animation(cast_animation, AbilityCastResult.new(ability_cast_result.ability, [ cast ]))
	
func on_partial_animation_finished():
	animations_finished += 1
	if (animations_started == animations_finished):
		on_animation_finished("impact")

func on_animation_finished(animation_name):
	if (animation_name == "impact"):
		emit_signal("animation_end")
