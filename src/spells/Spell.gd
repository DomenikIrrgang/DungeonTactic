extends "./Animation.gd"

const AbilityCastResult = preload("res://src/combat/AbilityCastResult.gd")

var reflected

func start(combat, ability_cast_result: AbilityCastResult):
	reflected = ability_cast_result.results[0].is_reflected()
	var source_info = combat.find_unit_info(ability_cast_result.results[0].source)
	var target_info = combat.find_unit_info(ability_cast_result.results[0].original_target)	
	run(combat.get_unit_group_info(source_info).position + source_info.model.get_center(),
		combat.get_unit_group_info(target_info).position + target_info.model.get_center()
	)
