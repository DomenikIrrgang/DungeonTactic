extends KinematicBody2D

var Dialog = preload("res://elements/dialog/Dialog.tscn")

export(String) var entity_name: String
export(bool) var interactable = true
export(PackedScene) var model_scene

export(NodePath) var user_interface_path
export(NodePath) var player_path

onready var player = get_node(player_path)
onready var user_interface = get_node(user_interface_path)
onready var interact_area = $Interactable
onready var interact_label = $InteractLabel

var model: Node2D

func _ready() -> void:
	set_model(model_scene)
	interact_area.enabled = interactable
	interact_label.visible = false
	interact_label.rect_position.y = -model.get_height() - interact_label.rect_size.y / 2
	if (interactable):
		interact_area.connect("interacted", self, "on_interact")
		interact_area.connect("entered_range", self, "_on_interact_range_entered")
		interact_area.connect("left_range", self, "_on_interact_range_left")
		
func get_model() -> Node2D:
	return model
	
func set_model(new_model: PackedScene) -> void:
	if (model != null):
		model.free()
	model = new_model.instance()
	add_child(model)
	
func _on_interact_range_entered(body) -> void:
	interact_label.visible = true
	
func _on_interact_range_left(body) -> void:
	interact_label.visible = false

# Override this to get called when the player interacts with this entity
func _on_interact():
	pass
	
func open_dialog(message: String) -> Control:
	var dialog = Dialog.instance()
	player.prevent_movement()
	dialog.connect("dialog_finished", self, "_on_dialog_finished")
	user_interface.add_child(dialog)
	dialog.show_message(message, entity_name)
	return dialog
	
func _on_dialog_finished(message: String) -> void:
	player.allow_movement()
