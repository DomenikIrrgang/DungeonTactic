extends "./Entity.gd"

export(float) var speed

var velocity = Vector2()

func _unhandled_input(event):
	if (Input.is_action_pressed("ui_right")):
		self.velocity.x = Input.get_action_strength("ui_right")
	elif (Input.is_action_pressed("ui_left")):
		self.velocity.x = -Input.get_action_strength("ui_left")
	else:
		self.velocity.x = 0
	if (Input.is_action_pressed("ui_down")):
		self.velocity.y = Input.get_action_strength("ui_down")
	elif (Input.is_action_pressed("ui_up")):
		self.velocity.y = -Input.get_action_strength("ui_up")
	else:
		self.velocity.y = 0

func _physics_process(delta: float) -> void:
	move_and_slide(self.velocity.normalized() * speed * delta)
	
func prevent_movement() -> void:
	set_process_unhandled_input(false)
	self.velocity.x = 0
	self.velocity.y = 0
	
func allow_movement() -> void:
	set_process_unhandled_input(true)
