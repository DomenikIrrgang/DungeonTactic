extends "./Ability.gd"

func _init().("Fireball", 0):
	pass

func get_tooltip():
	return "Sends a fireball to the target."
	
func get_spell_school():
	return SpellSchool.FIRE
	
func can_crit():
	return true
	
func get_critical_chance():
	return 0
	
func get_critical_effect():
	return 1
	
func get_miss_chance():
	return 0
	
func can_miss():
	return true
	
func is_reflectable():
	return false

func can_be_dodged():
	return true
	
func can_be_parried():
	return true
	
func can_be_resisted():
	return true
	
func get_resource_type():
	return ResourceType.MANA

func get_resource_cost():
	return 0
	
func can_be_reflected():
	return true
	
func get_target_type():
	return TargetType.AOE
	
func get_value(source, target):
	return 10
	
func get_scaling_factor(source, target):
	return 1.0
	
func get_animation():
	return preload("res://animations/abilities/FireballNova.tscn")

func execute(combat, source, target):
	pass
