var name
var cool_down
var remaining_cool_down

func _init(ability_name, ability_cool_down):
	name = ability_name
	cool_down = ability_cool_down
	remaining_cool_down = 0
	
func get_cool_down():
	return cool_down
	
func set_remaining_cool_down(new_remaining_cool_down):
	remaining_cool_down = new_remaining_cool_down

func get_remaining_cool_down():
	return remaining_cool_down
	
func get_name():
	return name
	
func reset():
	remaining_cool_down = 0
	
func execute(combat, source, target):
	assert(false, "Function not implemented yet!")
	
func get_tooltip():
	assert(false, "Function not implemented yet!")
	
func get_spell_school():
	assert(false, "Function not implemented yet!")
	
func can_crit():
	assert(false, "Function not implemented yet!")
	
func get_critical_chance():
	assert(false, "Function not implemented yet!")
	
func get_critical_effect():
	assert(false, "Function not implemented yet!")
	
func get_miss_chance():
	assert(false, "Function not implemented yet!")
	
func can_miss():
	assert(false, "Function not implemented yet!")
	
func is_reflectable():
	assert(false, "Function not implemented yet!")

func can_be_dodged():
	assert(false, "Function not implemented yet!")
	
func can_be_parried():
	assert(false, "Function not implemented yet!")
	
func can_be_reflected():
	assert(false, "Function not implemented yet!")
	
func can_be_resisted():
	assert(false, "Function not implemented yet!")
	
func get_resource_type():
	assert(false, "Function not implemented yet!")

func get_resource_cost():
	assert(false, "Function not implemented yet!")
	
func get_target_type():
	assert(false, "Function not implemented yet!")
	
func get_value(source, target):
	assert(false, "Function not implemented yet!")
	
func get_scaling_factor(source, target):
	assert(false, "Function not implemented yet!")

func get_animation():
	assert(false, "Function not implemented yet!")
