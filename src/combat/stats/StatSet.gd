var stats = {}

signal stat_set_stat_changed(stat, new_value)

func set_stat(stat: String, value: int):
	stats[stat] = value
	emit_signal("stat_set_stat_changed", stat, value)

func get_stat(stat: String):
	if !stats.has(stat):
		return 0
	return stats[stat]
	
func get_stats():
	return stats
	
func increase_stat(stat: String, value: int):
	set_stat(stat, get_stat(stat) + value)
	
func add_stat_set(stat_set):
	var result = get_script().new()
	for stat in get_stats():
		result.set_stat(stat, get_stat(stat))
	for stat in stat_set.get_stats():
		result.set_stat(stat, result.get_stat(stat) + stat_set.get_stat(stat))
	return result
