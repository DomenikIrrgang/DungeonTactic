var stat_set

func _init(stats):
	stat_set = stats
	
func get_stat_set():
	return stat_set
	
func get_health():
	return stat_set.get_stat(Stat.HEALTH) + stat_set.get_stat(Stat.STAMINA) * 10

func get_mana():
	return stat_set.get_stat(Stat.MANA) + stat_set.get_stat(Stat.INTELLECT) * 10
	
func get_dodge_chance():
	return (stat_set.get_stat(Stat.DODGE) + stat_set.get_stat(Stat.DEFENSE) * 0.25 + stat_set.get_stat(Stat.AGILITY) * 0.25) / 20.0
	
func get_parry_chance():
	return (stat_set.get_stat(Stat.PARRY) + stat_set.get_stat(Stat.DEFENSE) * 0.25) / 20.0

func get_critical_receive_chance():
	return stat_set.get_stat(Stat.DEFENSE) / 20.0

func get_miss_chance():
	return stat_set.get_stat(Stat.AVOIDANCE) / 20.0
	
func get_hit_chance():
	return (stat_set.get_stat(Stat.HIT) + stat_set.get_stat(Stat.INTELLECT) * 0.25) / 20.0

func get_haste():
	return stat_set.get_stat(Stat.HASTE) + stat_set.get_stat(Stat.AGILITY) / 20.0
	
func get_critical_chance():
	return (stat_set.get_stat(Stat.CRIT) + stat_set.get_stat(Stat.INTELLECT) * 0.25) / 20.0
	
func get_critical_effect():
	return (stat_set.get_stat(Stat.CRITICAL_EFFECT) + stat_set.get_stat(Stat.AGILITY) * 0.25) / 20.0
	
func get_spell_power(spell_school):
	return stat_set.get_stat(Stat.SPELL_POWER) + stat_set.get_stat(spell_school[0]) + stat_set.get_stat(Stat.INTELLECT)
	
func get_attack_power():
	return stat_set.get_stat(Stat.ATTACK_POWER) + stat_set.get_stat(Stat.AGILITY)
	
func get_resistance(spell_school):
	return stat_set.get_stat(spell_school[1]) + stat_set.get_stat(Stat.STAMINA)
	
func get_expertise():
	return stat_set.get_stat(Stat.EXPERTISE) / 20.0
	
func get_spell_reflect_chance():
	return stat_set.get_stat(Stat.SPELL_REFLECT) / 20.0

func get_mastery():
	return stat_set.get_stat(Stat.MASTERY)
	
func get_resource_reduction(resource_type: String):
	if(resource_type == ResourceType.HEALTH):
		return stat_set.get_stat(Stat.RESOURCE_COST_HEALTH)
	elif(resource_type == ResourceType.MANA):
		return stat_set.get_stat(Stat.RESOURCE_COST_MANA)
	elif(resource_type == ResourceType.RAGE):
		return stat_set.get_stat(Stat.RESOURCE_COST_RAGE)
	elif(resource_type == ResourceType.ENERGY):
		return stat_set.get_stat(Stat.RESOURCE_COST_ENERGY)
	pass
