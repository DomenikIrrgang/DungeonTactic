extends Node

const PHYSICAL = [ Stat.ATTACK_POWER, Stat.ARMOR ]
const FIRE = [ Stat.FIRE_SPELL_POWER, Stat.FIRE_RESISTANCE ]
const FROST = [ Stat.FROST_SPELL_POWER, Stat.FROST_RESISTANCE ]
const WIND = [ Stat.WIND_SPELL_POWER, Stat.WIND_RESISTANCE ]
const EARTH = [ Stat.EARTH_SPELL_POWER, Stat.EARTH_RESISTANCE ]
const LIGHT = [ Stat.LIGHT_SPELL_POWER, Stat.LIGHT_RESISTANCE ]
const SHADOW = [ Stat.SHADOW_SPELL_POWER, Stat.SHADOW_RESISTANCE ]
const NATURE = [ Stat.NATURE_SPELL_POWER, Stat.NATURE_RESISTANCE ]
const WATER = [ Stat.WATER_SPELL_POWER, Stat.WATER_RESISTANCE ]
const THUNDER = [ Stat.THUNDER_SPELL_POWER, Stat.THUNDER_RESISTANCE ]
const GRAVITY = [ Stat.GRAVITY_SPELL_POWER, Stat.GRAVITY_RESISTANCE ]

static func toArray():
	return [
		PHYSICAL,
		FIRE,
		FROST,
		WIND,
		EARTH,
		LIGHT,
		SHADOW,
		NATURE,
		WATER,
		THUNDER,
		GRAVITY
	]
