extends Node

# Increases attackpower, dodge, critical effect, haste.
const AGILITY = "AGILITY"

# Increases spellpower, mana, hit and crit by 1.
const INTELLECT = "INTELLECT"

# Increases the resistance to all spell schools by 1 and increases health by 10.
const STAMINA = "STAMINA"

# Increases the total health.
const HEALTH = "HEALTH"

# Increases effectiveness of physical abilities.
const ATTACK_POWER = "ATTACK_POWER"

# Increases the effect of all non physical abilities.
const SPELL_POWER = "SPELL_POWER"
const FIRE_SPELL_POWER = "FIRE_SPELL_POWER"
const FROST_SPELL_POWER = "FROST_SPELL_POWER"
const WIND_SPELL_POWER = "WIND_SPELL_POWER"
const EARTH_SPELL_POWER = "EARTH_SPELL_POWER"
const LIGHT_SPELL_POWER = "LIGHT_SPELL_POWER"
const SHADOW_SPELL_POWER = "SHADOW_SPELL_POWER"
const NATURE_SPELL_POWER = "NATURE_SPELL_POWER"
const WATER_SPELL_POWER = "WATER_SPELL_POWER"
const THUNDER_SPELL_POWER = "THUNDER_SPELL_POWER"
const GRAVITY_SPELL_POWER = "GRAVITY_SPELL_POWER"

# Reduces the effect of abilities on the unit.
const ARMOR = "ARMOR"
const FIRE_RESISTANCE = "FIRE_RESISTANCE"
const FROST_RESISTANCE = "FROST_RESISTANCE"
const WIND_RESISTANCE = "WIND_RESISTANCE"
const EARTH_RESISTANCE = "EARTH_RESISTANCE"
const LIGHT_RESISTANCE = "LIGHT_RESISTANCE"
const SHADOW_RESISTANCE = "SHADOW_RESISTANCE"
const NATURE_RESISTANCE = "NATURE_RESISTANCE"
const WATER_RESISTANCE = "WATER_RESISTANCE"
const THUNDER_RESISTANCE = "THUNDER_RESISTANCE"
const GRAVITY_RESISTANCE = "GRAVITY_RESISTANCE"

# Increases the chance to dodge an ability. Some abilities can not be dodged tho.
const DODGE = "DODGE"

# Increases the chance to parry an attack (physical). If an attack has been parried, a counter attack is executed.
const PARRY = "PARRY"

# Increases the chance for an ability missing on you.
const AVOIDANCE = "AVOIDANCE"

# Increases the chance to not miss with abilities.
const HIT = "HIT"

# Reduces the chance that abilities are dodged or parried.
const EXPERTISE = "EXPERTISE"

# Reduces the chance for abilities to crit on you and increases your dodge and parry.
const DEFENSE = "DEFENSE"

# Increases the chance for abilities to critically strike.
const CRIT = "CRIT"

# Effects different abilities and resources. Also determines who attacks first.
const HASTE = "HASTE"

# Enhances the effect of the mastery crystal.
const MASTERY = "MASTERY"

# Increases the effect of critical strikes.
const CRITICAL_EFFECT = "CRITICAL_EFFECT"

# Increases maximum value of the proper resource.
const RAGE = "RAGE"
const ENERGY = "ENERGY"
const MANA = "MANA"

# Reduces costs of abilities.
const RESOURCE_COST_MANA = "RESOURCE_COST_MANA"
const RESOURCE_COST_HEALTH = "RESOURCE_COST_HEALTH"
const RESOURCE_COST_RAGE = "RESOURCE_COST_RAGE"
const RESOURCE_COST_ENERGY = "RESOURCE_COST_ENERGY"

# Effects ability value directly.
const DAMAGE_DONE = "DAMAGE_DONE"
const DAMAGE_TAKEN = "DAMAGE_TAKEN"
const HEALING_DONE = "HEALING_DONE"
const HEALING_TAKEN = "HEALING_TAKEN"

# Gives a chance to reflect spells
const SPELL_REFLECT = "SPELL_REFLECT"

static func toArray():
	return [
		AGILITY,
		INTELLECT,
		STAMINA,
		HEALTH,
		ATTACK_POWER,
		SPELL_POWER,
		FIRE_SPELL_POWER,
		FROST_SPELL_POWER,
		WIND_SPELL_POWER,
		EARTH_SPELL_POWER,
		LIGHT_SPELL_POWER,
		SHADOW_SPELL_POWER,
		NATURE_SPELL_POWER,
		WATER_SPELL_POWER,
		THUNDER_SPELL_POWER,
		GRAVITY_SPELL_POWER,
		ARMOR,
		FIRE_RESISTANCE,
		FROST_RESISTANCE,
		WIND_RESISTANCE,
		EARTH_RESISTANCE,
		LIGHT_RESISTANCE,
		SHADOW_RESISTANCE,
		NATURE_RESISTANCE,
		WATER_RESISTANCE,
		THUNDER_RESISTANCE,
		GRAVITY_RESISTANCE,
		DODGE,
		PARRY,
		AVOIDANCE,
		HIT,
		EXPERTISE,
		DEFENSE,
		CRIT,
		HASTE,
		MASTERY,
		CRITICAL_EFFECT,
		RAGE,
		MANA,
		ENERGY,
		RESOURCE_COST_MANA,
		RESOURCE_COST_HEALTH,
		RESOURCE_COST_ENERGY,
		RESOURCE_COST_RAGE,
		DAMAGE_DONE,
		DAMAGE_TAKEN,
		HEALING_DONE,
		HEALING_TAKEN,
		SPELL_REFLECT
	]
