extends Node

const MANA = "Mana"
const RAGE = "Rage"
const ENERGY = "Energy"
const HEALTH = "Health"

static func toArray():
	return [
		HEALTH,
		MANA,
		RAGE,
		ENERGY
	]
