extends "Resource.gd"

var stat_calculator

func _init(calculator):
	stat_calculator = calculator
	type = ResourceType.RAGE
	set_maximum_value(100)
	stat_calculator.get_stat_set().connect("stat_set_stat_changed", self, "_on_stats_changed")

func reset():
	set_value(0)
	
func get_maximum_value():
	return .get_maximum_value() + stat_calculator.get_stat_set().get_stat(Stat.RAGE)
	
func _on_stats_changed(_stat, _value):
	emit_signal("resource_maximum_value_changed", get_maximum_value(), get_maximum_value())

func clone():
	var new_resource = ResourceLoader.load("res://src/combat/resources/Rage.gd").new(stat_calculator)
	new_resource.type = type
	new_resource.value = value
	new_resource.maximum_value = maximum_value
	return new_resource
