var value = 50
var maximum_value = 100
var type = ResourceType.HEALTH

signal resource_value_changed(new_value, old_value)
signal resource_maximum_value_changed(new_maximum_value, old_maximum_value)

func set_value(new_value):
	var old_value = value
	if new_value >= 0:
		if new_value <= get_maximum_value():
			value = new_value
		else:
			value = get_maximum_value()
	else:
		value = 0
	emit_signal("resource_value_changed", value, old_value)
	return value - old_value
	
func set_maximum_value(new_maximum_value):
	var old_maximum_value = maximum_value
	maximum_value = new_maximum_value if new_maximum_value > 0 else 0
	value = value if value <= get_maximum_value() else get_maximum_value()
	emit_signal("resource_maximum_value_changed", maximum_value, old_maximum_value)
	return maximum_value - old_maximum_value
	
func get_maximum_value():
	return maximum_value
	
func get_value():
	return value
	
func increase_value(change):
	return set_value(get_value() + change)

func decrease_value(change):
	return increase_value(-change)
	
func increase_maximum_value(change):
	return set_maximum_value(get_maximum_value() + change)
	
func decrease_maximum_value(change):
	return increase_maximum_value(-change)
	
func clone():
	var new_resource = ResourceLoader.load("res://src/combat/resources/Resource.gd").new()
	new_resource.type = type
	new_resource.value = value
	new_resource.maximum_value = maximum_value
	return new_resource

func reset():
	set_value(get_maximum_value())
