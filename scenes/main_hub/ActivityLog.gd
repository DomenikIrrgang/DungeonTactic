extends Control

var FadingLabel = preload("res://elements/FadingLabel.tscn")

func _ready():
	Inventory.connect("inventory_item_added", self, "on_inventory_item_added")
	Experience.connect("experience_gained", self, "on_experience_gained")
	
func on_inventory_item_added(item: Item, quantity: int, index: int) -> void:
	var message: String
	if (quantity > 1):
		message= "+ %dx %s" % [
			quantity,
			item.name
		]
	else:
		message = "+ %s" % [ item.name ]
	add_label(message)
	
func label_faded(label) -> void:
	remove_child(label)
	
func add_label(message: String) -> void:
	var label = FadingLabel.instance()
	label.text = message
	label.align = Label.ALIGN_RIGHT
	label.connect("label_faded", self, "label_faded")
	add_child(label)
	
func on_experience_gained(amount: int) -> void:
	add_label("+ " + String(amount) + " experience")
