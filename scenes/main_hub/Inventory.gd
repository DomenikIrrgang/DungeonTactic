extends Control

const InventorySlot = preload("res://scenes/main_hub/InventorySlot.tscn")

onready var grid = $Grid
onready var menu = $PopupMenu

var inventory_slots: Array

func _ready() -> void:
	self.visible = false
	Inventory.connect("inventory_item_added", self, "on_item_added")
	Inventory.connect("inventory_item_placed", self, "on_item_added")
	inventory_slots = []
	inventory_slots.resize(Inventory.size)
	menu.add_item("Move", 1)
	menu.add_item("Delete", 2)
	menu.add_item("Cancel", 3)
	for i in range(0, Inventory.size):
		var inventory_slot = InventorySlot.instance()
		inventory_slot.set_index(i)
		inventory_slots[i] = inventory_slot
		grid.add_child(inventory_slot)
		if (Inventory.get_items()[i] != null):
			inventory_slot.set_item(Inventory.get_items()[i].item, Inventory.get_items()[i].quantity, i)

func on_item_added(item: Item, quantity: int, index: int) -> void:
	if (item != null):
		inventory_slots[index].set_item(item, Inventory.get_items()[index].quantity, index)
	else:
		inventory_slots[index].clear()
	
func _input(event):
	if (event.is_action_released("toggle_inventory")):
		toggle_inventory()
	if (is_open() and menu.visible == false and event.is_action_released("interact")):
		menu.rect_position = rect_position + get_focus_owner().rect_position
		menu.popup()

func _unhandled_input(event):
	if (is_open()):
		get_tree().set_input_as_handled()
		
func toggle_inventory() -> bool:
	set_open(!is_open())
	return is_open()
		
func set_open(open: bool) -> void:
	self.visible = open
	if (is_open()):
		inventory_slots[0].grab_focus()
		
func is_open() -> bool:
	return self.visible
