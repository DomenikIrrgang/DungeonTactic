extends Control
class_name InventorySlot

onready var border = $Border
onready var icon = $Icon
onready var quantity = $Quantity

var index: int
var item: Item

func _ready() -> void:
	clear()

func _on_InventorySlot_mouse_entered():
	self.modulate.r += 0.1
	self.modulate.g += 0.1
	self.modulate.b += 0.1

func _on_InventorySlot_mouse_exited():
	self.modulate.r -= 0.1
	self.modulate.g -= 0.1
	self.modulate.b -= 0.1
	
func set_index(index: int) -> void:
	self.index = index

func set_item(item: Item, quantity: int, index: int) -> void:
	self.item = item
	if (item != null):
		self.icon.texture = load(item.icon)
		self.quantity.text = String(quantity)
		self.quantity.visible = true
	else:
		clear()
		
func clear() -> void:
	self.quantity.visible = false
	self.quantity.text = ""
	self.icon.texture = null
	
func can_drop_data(position, data):
	return true
	
func drop_data(position, data):
	Inventory.swap_items(data.index, self.index)
	
func get_drag_data(position):
	var drag_texture = TextureRect.new()
	drag_texture.expand = true
	drag_texture.texture = self.icon.texture
	drag_texture.rect_size = self.rect_size
	var control = Control.new()
	control.add_child(drag_texture)
	drag_texture.rect_position = -0.5 * drag_texture.rect_size
	set_drag_preview(control)
	return { "index": index, "item": item }


func _on_InventorySlot_focus_entered():
	border.modulate.r *= 10
	border.modulate.g *= 10
	border.modulate.b *= 10

func _on_InventorySlot_focus_exited():
	border.modulate.r /= 10
	border.modulate.g /= 10
	border.modulate.b /= 10
