extends TextureRect

export(String) var action_name

func _input(event) -> void:
	if (event.is_action_released(action_name)):
		self.rect_scale -= Vector2(0.3, 0.3)
	if (event.is_action_pressed(action_name)):
		self.rect_scale += Vector2(0.3, 0.3)

func _ready():
	Input.connect("joy_connection_changed", self, "_joy_connection_changed")
	set_button_texture(InputControlls.get_button_texture(action_name))
	
func _joy_connection_changed(device_id: int, connected: bool):
	set_button_texture(InputControlls.get_button_texture(action_name))

func set_button_texture(texture_path: String) -> void:
	self.texture = load(texture_path)
