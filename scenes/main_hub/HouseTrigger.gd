extends Area2D

var Dialog = preload("res://elements/dialog/Dialog.tscn")

export(String) var author: String
export(String) var text: String

onready var user_interface = $"../UserInterface"
onready var player = $"../Objects/Player"

var dialog: Node

func _on_HouseTrigger_body_entered(body):
	dialog = Dialog.instance()
	user_interface.add_child(dialog)
	player.prevent_movement()
	dialog.connect("dialog_finished", self, "_on_dialog_finished")
	dialog.show_message(text, author)

func _on_HouseTrigger_body_exited(body):
	dialog.free()

func _on_dialog_finished(message) -> void:
	SceneSwitcher.change_scene("res://scenes/main_hub/MainHub.tscn")
