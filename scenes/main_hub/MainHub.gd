extends Node2D

export(Color) var day_color
export(Color) var night_color
export(int) var time_speed = 1

signal entered_day()
signal entered_night()

onready var time_tween = $TimeTween

var time = 0
var day_phase: String

onready var canvas_modulate = $CanvasModulate

func _ready() -> void:
	night()
	
func _process(delta) -> void:
	time += delta * time_speed
	if (get_time_of_day() > 18 or get_time_of_day() < 6):
		night()
	else:
		day()
	
func get_time_of_day() -> float:
	return float((int(time) % (24 * 10)) / 10.0)

func day() -> void:
	if (day_phase != "day"):
		day_phase = "day"
		time_tween.interpolate_property(canvas_modulate, "color", canvas_modulate.color, day_color, 2)
		time_tween.start()
	
func night() -> void:
	if (day_phase != "night"):
		day_phase = "night"
		time_tween.interpolate_property(canvas_modulate, "color", canvas_modulate.color, night_color, 2)
		time_tween.start()
