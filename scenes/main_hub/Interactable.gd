extends Area2D

export(float) var collision_width: float
export(float) var collision_height: float
export(bool) var enabled: bool

onready var interactable_area = $InteractableArea

var in_interact_range: bool

signal interacted()
signal entered_range(body)
signal left_range(body)

func _ready() -> void:
	interactable_area.shape.extents.x = collision_width
	interactable_area.shape.extents.y = collision_height
	pass

func _on_Interactable_body_entered(body) -> void:
	in_interact_range = true
	if (enabled):
		emit_signal("entered_range", body)

func _on_Interactable_body_exited(body) -> void:
	in_interact_range = false
	if (enabled):
		emit_signal("left_range", body)

func _input(event) -> void:
	if (enabled and event.is_action_released("interact") and in_interact_range):
		emit_signal("interacted")
