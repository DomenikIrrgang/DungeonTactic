extends Control

export(String, FILE, "*.tscn") var combat_scene
export(float) var menu_transition_time = 1

var menu_origin_position = Vector2.ZERO
var menu_origin_size = Vector2.ZERO

var current_menu
var menu_stack = []

onready var menu_1 = $MainMenu
onready var menu_2 = $SecondMenu
onready var tween = $Tween

func _ready():
	menu_origin_position = Vector2(0, 0)
	menu_origin_size = get_viewport_rect().size
	current_menu = menu_1
	
func move_to_next_menu(next_menu_id: String):
	var next_menu = get_menu_from_menu_id(next_menu_id)
	tween.interpolate_property(current_menu, "rect_global_position", current_menu.rect_global_position, Vector2(-menu_origin_size.x, 0), menu_transition_time)
	tween.interpolate_property(next_menu, "rect_global_position", next_menu.rect_global_position, menu_origin_position, menu_transition_time)
	tween.start()
	menu_stack.append(current_menu)
	current_menu = next_menu
	
func move_to_previous_menu():
	var previous_menu = menu_stack.pop_back()
	if previous_menu != null:
		tween.interpolate_property(previous_menu, "rect_global_position", previous_menu.rect_global_position, Vector2(menu_origin_position.x, 0), menu_transition_time)
		tween.interpolate_property(current_menu, "rect_global_position", current_menu.rect_global_position, Vector2(menu_origin_size.x, 0), menu_transition_time)
		tween.start()
		current_menu = previous_menu
	
	
func get_menu_from_menu_id(menu_id: String):
	match menu_id:
		"menu_1":
			return menu_1
		"menu_2":
			return menu_2
		_:
			return menu_1

func _on_Button_pressed():
	move_to_next_menu("menu_2")


func _on_Button4_pressed():
	move_to_previous_menu()


func _on_Button2_pressed():
	var scene_change_result = get_tree().change_scene(combat_scene)
