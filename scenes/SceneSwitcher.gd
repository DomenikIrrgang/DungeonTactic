extends Node

const SceneSwitchAnimation = preload("res://scenes/scene_switch/SceneSwitchAnimation.tscn")

onready var scene_switch_animation = SceneSwitchAnimation.instance()

var _params: Dictionary

func change_scene(next_scene, params = {}) -> void:
	_params = params
	get_tree().get_root().add_child(scene_switch_animation)
	scene_switch_animation.show_transition("Circle_Out")
	yield(scene_switch_animation, "transition_end")
	scene_switch_animation.show_transition("Circle_In")
	get_tree().change_scene(next_scene)

func get_param(name: String):
	if _params != null and _params.has(name):
		return _params[name]
	return null
