extends Control

const Ghost = preload("res://src/combat/enemies/Ghost.gd")
const Rat = preload("res://src/combat/enemies/Rat.gd")
const Snake = preload("res://src/combat/enemies/Snake.gd")

export(String, FILE, "*.tscn") var demo_combat

func _ready() -> void:
	$CenterContainer/MenuItems/DemoCombat.grab_focus()

func _on_DemoCombat_pressed():
	SceneSwitcher.change_scene(demo_combat, {
		"player_group": [
			Ghost.new(1),
			Ghost.new(2),
			Rat.new(3),
			Rat.new(4)
		],
		"enemy_group": [
			Ghost.new(1),
			Ghost.new(2),
			Rat.new(3),
			Rat.new(4)
		]
	})


func _on_Exit_pressed():
	get_tree().quit()

func _on_Settings_pressed():
	SceneSwitcher.change_scene("res://scenes/main_hub/MainHub.tscn")
