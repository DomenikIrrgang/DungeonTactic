extends Node

export(NodePath) var node
export(bool) var enabled

func _input(event) -> void:
	if (event is InputEventKey or event is InputEventMouseButton) and enabled:
		self.enabled = false
		self.get_parent().visible = false
		var menu = get_node(node)
		menu.visible = true
