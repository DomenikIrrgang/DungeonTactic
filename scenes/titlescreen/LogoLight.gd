extends Node2D

export(float) var rotation_speed = 0.5
export(float) var movement_speed_x = 250
export(float) var movement_speed_y = 50
export(int) var random_speed_x = 300
export(int) var random_speed_y = 50


export(int) var x_limit_low = -250
export(int) var x_limit_high = 250
export(int) var y_limit_low = -200
export(int) var y_limit_high = 200

export(float) var curve_factor = 2.0

export(NodePath) var tween_path

var texture_scale_fading = true

var initial_speed_x: int
var initial_speed_y: int

var tween: Tween
var fairy

func _ready() -> void:
	initial_speed_x = movement_speed_x
	initial_speed_y = movement_speed_y
	tween = $"../FairyMovement/Tween"
	fairy = $Fairy

func _process(delta) -> void:
	if (self.position.x > x_limit_high and movement_speed_x >= initial_speed_x) or (self.position.x < x_limit_low and movement_speed_x <=  -initial_speed_x):
		change_direction_x()
	if (self.position.y > y_limit_high and movement_speed_y >= initial_speed_y) or (self.position.y < y_limit_low and movement_speed_y <=  -initial_speed_y):
		change_direction_y()
	if (randf() < 0.07 * delta):
		change_direction_x()
	if (randf() < 0.07 * delta):
		change_direction_y()
	self.position.x += movement_speed_x * delta
	self.position.y += movement_speed_y * delta
	
func change_direction_x() -> void:
	var target_speed = 0
	if (movement_speed_x < 0):
		target_speed = initial_speed_x + random_speed_x * randf()
	else:
		target_speed = -initial_speed_x - random_speed_x * randf()
	tween.interpolate_property(self, "movement_speed_x", movement_speed_x, target_speed, curve_factor)
	tween.start()
	if (target_speed > 0):
		fairy.set_animation("idle")
	if (target_speed <= 0):
		fairy.set_animation("idle")
		
func change_direction_y() -> void:
	var target_speed = 0
	if (movement_speed_y < 0):
		target_speed = initial_speed_y + random_speed_y * randf()
	else:
		target_speed = -initial_speed_y - random_speed_y * randf()
	tween.interpolate_property(self, "movement_speed_y", movement_speed_y, target_speed, curve_factor)
	tween.start()
	
