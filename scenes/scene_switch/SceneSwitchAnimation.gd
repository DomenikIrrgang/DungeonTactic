extends Node2D

onready var animation = $CanvasLayer/AnimationPlayer
onready var rectangle = $CanvasLayer/SceneSwitchRect
onready var circle = $CanvasLayer/Circle

signal transition_start(transition_name)
signal transition_end(transition_name)

func show_transition(transition_name: String) -> void:
	if (transition_name == "Fade_In" or transition_name == "Fade_Out"):
		circle.visible = false
		rectangle.visible = true
	else:
		circle.visible = true
		rectangle.visible = false
	animation.play(transition_name)

func _on_AnimationPlayer_animation_finished(animation_name):
	emit_signal("transition_end", animation_name)

func _on_AnimationPlayer_animation_started(animation_name):
	emit_signal("transition_start", animation_name)
