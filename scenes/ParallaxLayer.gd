extends ParallaxLayer

export(float) var background_speed

func _process(delta) -> void:
	self.motion_offset.x += delta * background_speed
