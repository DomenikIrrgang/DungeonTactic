extends Node2D

const Unit = preload("res://src/combat/Unit.gd")
const CombatLogic = preload("res://src/combat/CombatLogic.gd")
const Fireball = preload("res://src/combat/abilities/Fireball.gd")

export(int) var model_padding_x = 20
export(int) var model_padding_y = 20

var characters = []
var enemies = []

var player_group: Array
var enemy_group: Array

var logic: CombatLogic
var combat_results: Array

func init():
	pass

func _ready():
	player_group = SceneSwitcher.get_param("player_group")
	enemy_group = SceneSwitcher.get_param("enemy_group")
	init_combat(CombatLogic.new(player_group, enemy_group))
	
func spawn_character(unit):
	var character = { "unit": unit, "model": unit.model.instance()}
	characters.append(character)
	get_character_resource_displays()[characters.size() - 1].init(unit)
	set_character_model(characters.size() - 1, character.model)
	
func set_character_model(position, model):
	model.set_animation("idle")
	model.position.x = -model_padding_x * position
	model.position.y = model_padding_y * position
	$PlayerUnits.add_child(model)
	
func spawn_enemy(unit):
	var enemy = { "unit": unit, "model": unit.model.instance()}
	enemies.append(enemy)
	get_enemy_resource_displays()[enemies.size() - 1].init(unit)
	set_enemy_model(enemies.size() - 1, enemy.model)
	
func set_enemy_model(position, model):
	model.flip()
	model.set_animation("idle")
	model.position.x = model_padding_x * position
	model.position.y = model_padding_y * position
	$EnemyUnits.add_child(model)
	
func init_combat(combat: CombatLogic):
	logic = combat
	logic.connect("round_over", get_round_counter(), "_on_round_over")
	get_message_box().set_text("Choose your actions")
	for player in combat.player_group:
		spawn_character(player)
	for enemy in combat.enemy_group:
		spawn_enemy(enemy)
	
func run_round():
	var results = logic.calculate_round()
	for result in results:
		$UserInterface/Log/Tabs/Combat.add_combat_log_result(logic.round_counter - 1, result)
		if (result.type == CombatResultType.RESOURCE_UPDATE):
			for resource_change in result.data:
				var resource_display = get_unit_resource_display(resource_change.unit)
				resource_display.init(resource_change.unit)
		if (result.type == CombatResultType.ABILITY_CAST_RESULT):
			yield(animate_result(result.data[0]), "animation_end")
			for cast in result.data[0].results:
				find_unit_info(cast.target).model.ability_landed(cast)
		yield(get_tree().create_timer(0.5), "timeout")

func animate_result(result):
	var animation = result.ability.get_animation().instance()
	return start_animation(animation, result)
	
func start_animation(animation, result):
	add_child(animation)
	animation.start(
		self,
		result)
	return animation
	
func get_unit_group_info(unit):
	return $PlayerUnits if (characters.has(unit)) else $EnemyUnits
		
func find_unit_info(unit):
	for character in characters:
		if character.unit == unit:
			return character
	for enemy in enemies:
		if enemy.unit == unit:
			return enemy
	return null
	
func _on_Button_pressed():
	logic.add_combat_action(Fireball.new(), player_group[0], enemy_group[0])
	run_round()
	
func set_message(text):
	get_message_box().set_text(text)
	
func get_unit_resource_display(unit):
	for index in range(characters.size()):
		if (characters[index].unit.id == unit.id):
			return get_character_resource_displays()[index]
	for index in range(enemies.size()):
		if (enemies[index].unit.id == unit.id):
			return get_enemy_resource_displays()[index]
	return null

func get_enemy_resource_displays():
	return  [$UserInterface/EnemyResources/EnemyResource1, $UserInterface/EnemyResources/EnemyResource2, $UserInterface/EnemyResources/EnemyResource3, $UserInterface/EnemyResources/EnemyResource4]
	
func get_character_resource_displays():
	return  [$UserInterface/PlayerResources/UnitResource1, $UserInterface/PlayerResources/UnitResource2, $UserInterface/PlayerResources/UnitResource3, $UserInterface/PlayerResources/UnitResource4]

func get_round_counter():
	return $UserInterface/RoundCounter
	
func get_message_box():
	return $UserInterface/MessageBox
