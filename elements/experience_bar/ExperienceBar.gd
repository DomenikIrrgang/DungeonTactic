extends Control

export(float) var animation_speed = 0.8

onready var label = $TextPosition/Text
onready var bar = $Bar
onready var tween = $Tween

func _ready() -> void:
	update_bar()
	Experience.connect("experience_gained", self, "experience_changed")
	Experience.connect("level_up", self, "experience_changed")

func experience_changed(amount):
	update_bar()
	
func update_bar() -> void:
	label.text = "%s / %s" % [
		Experience.get_current_experience(),
		Experience.get_current_experience_needed()
	]
	tween.interpolate_property(bar, 'value', bar.value, Experience.get_current_experience() * 100 / Experience.get_current_experience_needed(), animation_speed, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.start()
