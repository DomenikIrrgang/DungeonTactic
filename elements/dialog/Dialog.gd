extends Control

export(float) var text_speed = 100
export(int) var max_message_length = 260

onready var tween = $Tween
onready var message_label = $HBoxContainer/VBoxContainer/Background/Text
onready var arrow = $HBoxContainer/VBoxContainer/Background/Arrow
onready var author_label = $HBoxContainer/VBoxContainer/Background/AuthorLabel

var author: String
var full_message: String
var message_parts: Array
var message_counter: int
var current_message_shown: bool

signal dialog_started(author, message)
signal dialog_next_message_part(message, part, partNumber, totalNumberOfParts)
signal dialog_finished(message)

func _ready() -> void:
	arrow.visible = false
	tween.connect("tween_all_completed", self, "text_completed")
	
func _input(event) -> void:
	if (visible):
		if (event.is_action_released("ui_accept") and is_last_message_part() and current_message_shown):
			hide()
		elif (event.is_action_released("ui_accept") and !is_last_message_part() and current_message_shown):
			show_next_message()
		get_tree().set_input_as_handled()
			

func show_message(message: String, author_name: String = "") -> void:
	author = author_name
	full_message = message
	if (author.length() > 0):
		author_label.text = author
		author_label.visible = true
	else:
		author_label.visible = false
	message_parts = split_message(full_message, max_message_length)
	message_counter = 0
	show_message_part(message_parts[message_counter])
	show()
	emit_signal("dialog_started", author, message)
		
func show_next_message() -> void:
	message_counter += 1
	show_message_part(message_parts[message_counter])

func is_last_message_part() -> bool:
	return message_counter >= message_parts.size() - 1
	
func show_message_part(part: String) -> void:
	current_message_shown = false
	arrow.visible = false
	var speed = part.length() / text_speed
	message_label.text = part
	tween.interpolate_property(message_label, "percent_visible", 0, 1, speed)
	tween.start()
	emit_signal("dialog_next_message_part", part, message_counter, message_parts.size())
	
func text_completed() -> void:
	current_message_shown = true
	show_arrow()
	
func show_arrow():
	arrow.visible = true

func hide_arrow():
	arrow.visible = false
	
func split_message(message: String, chunk_size: int) -> Array:
	if (message.length() == 0):
		return []
	var end_position = chunk_size
	if (chunk_size < message.length()):
		end_position = message.substr(0, end_position).find_last(" ")
	var chunk = message.substr(0, end_position)
	message = message.substr(end_position, message.length())
	return [ chunk ] + split_message(message, chunk_size)
	
func show() -> void:
	self.visible = true
	
func hide() -> void:
	message_counter = 0
	message_parts = []
	tween.remove(message_label)
	self.visible = false
	emit_signal("dialog_finished", full_message)
