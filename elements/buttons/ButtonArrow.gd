extends CanvasItem

export(float) var speed: float
export(float) var minimum_fade: float
export(float) var maximum_fade: float
export(bool) var fading: bool

func _ready():
	if fading == true:
		modulate.a = 1
	else:
		modulate.a = 0

func _process(delta):
	if fading == true:
		modulate.a -= speed * delta
		if modulate.a <= minimum_fade:
			fading = false
	else:
		modulate.a += speed * delta
		if modulate.a >= maximum_fade:
			fading = true
	
