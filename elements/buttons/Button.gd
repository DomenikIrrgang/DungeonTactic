extends TextureButton

export(float) var scaling_speed: float
export(float) var minimum_scale: float
export(float) var maximum_scale: float
export(bool) var pulse_font: bool
export(bool) var scaling_up: bool

export(String) var text: String

var button_press_indend = 3

func _ready():
	$Text.text = text
	$Arrow.visible = false
	
func _process(delta: float) -> void:
	if (self.has_focus() and pulse_font):
		if scaling_up == true:
			$Text.rect_scale.x += scaling_speed * delta
			$Text.rect_scale.y += scaling_speed * delta
			if $Text.rect_scale.x >= maximum_scale:
				scaling_up = false
		else:
			$Text.rect_scale.x -= scaling_speed * delta
			$Text.rect_scale.y -= scaling_speed * delta
			if $Text.rect_scale.x <= minimum_scale:
				scaling_up = true


func _on_Button_focus_entered():
	$Arrow.visible = true


func _on_Button_focus_exited():
	$Arrow.visible = false
	$Text.rect_scale = Vector2(minimum_scale,minimum_scale)


func _on_Button_button_down():
	$Text.rect_position.x += button_press_indend
	$Text.rect_position.y += button_press_indend


func _on_Button_button_up():
	$Text.rect_position.x -= button_press_indend
	$Text.rect_position.y -= button_press_indend

