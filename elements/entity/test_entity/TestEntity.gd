extends "res://src/overworld/Entity.gd"

const Ghost = preload("res://src/combat/enemies/Ghost.gd")
const Rat = preload("res://src/combat/enemies/Rat.gd")
const Snake = preload("res://src/combat/enemies/Snake.gd")

func on_interact() -> void:
	var dialog = open_dialog("Buuuuhuuu!")
	model.set_animation("dodge")
	dialog.connect("dialog_finished", self, "morph_player")
	
func morph_player(message: String) -> void:
	player.model.hit()
	Experience.increase(randi() % 200)
	Inventory.add_item(InsigniaOfDoom.new(), randi() % 3 + 1)
	
func start_combat(message: String) -> void:
	SceneSwitcher.change_scene(Scenes.COMBAT, {
		"player_group": [
			Ghost.new(1),
			Ghost.new(2),
			Rat.new(3),
			Rat.new(4)
		],
		"enemy_group": [
			Ghost.new(1),
			Ghost.new(2),
			Rat.new(3),
			Rat.new(4)
		]
	})
