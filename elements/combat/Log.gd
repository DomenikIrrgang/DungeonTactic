extends ScrollContainer

var SmallLabel = preload("res://elements/VerySmallLabel.tscn")
var CombatResult = preload("res://src/combat/CombatResult.gd")

export(NodePath) var scroll_pane

var previous_scroll_max_value = 0

func _ready():
	get_v_scrollbar().connect("changed", self, "scroll_changed")
	pass
	
func add_line(text: String):
	var message = Label.new()
	message.text = text
	message.autowrap = true
	$CombatMessages.add_child(message)
	pass
	
func scroll_changed():
	if previous_scroll_max_value != get_v_scrollbar().max_value:
		scroll_vertical = int(get_v_scrollbar().max_value)
		previous_scroll_max_value = get_v_scrollbar().max_value

func add_combat_log_line(turn, text):
	add_line("[Round " + str(turn) + "] " + text)

func add_combat_log_result(turn, result):
	if (result.type == CombatResultType.RESOURCE_UPDATE):
		for resource_update in result.data:
			add_combat_log_line(turn, resource_update.unit.name + "s " + resource_update.type + " changed by " + str(resource_update.value))
	elif (result.type == CombatResultType.ABILITY_CAST_START):
		add_combat_log_line(turn, result.data[0].unit.name + " starts casting " + result.data[0].ability.name)
	elif (result.type == CombatResultType.ABILITY_CAST_RESULT):
		var cast_results = result.data[0]
		for cast_result in cast_results.results:
			var text_placeholder = "%ss %s hits %s for %s (type: %s, resisted: %s, reflected: %s, spellschool: %s)"
			var text = text_placeholder % [
				cast_result.source.name,
				cast_results.ability.name,
				cast_result.target.name,
				cast_result.value,
				cast_result.hit_type,
				cast_result.resist_amount,
				cast_result.is_reflected(),
				cast_results.ability.get_spell_school()[0]
			]
			add_combat_log_line(turn, text)
	elif (result.type == CombatResultType.ABILITY_CAST_FAILED_RESOURCE_MISSING):
		var text = "%s does not have enough %s to cast %s" % [
			result.data[0].unit.name,
			result.data[0].ability.get_resource_type(),
			result.data[0].ability.get_name(),
		]
		add_combat_log_line(turn, text)
	else:
		add_line(result.type)
