extends "res://src/util/unit_util.gd"

func _ready():
	$Sprite.modulate.r = random_value(0, 0.5)
	$Sprite.modulate.g = random_value(0, 0.5)
	$Sprite.modulate.b = random_value(0, 0.5)
	
func random_value(minimum: float, maximum: float):
	return randf() * (maximum - minimum) + minimum
